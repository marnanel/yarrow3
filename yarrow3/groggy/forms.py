from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
import yarrow3.groggy.models as groggy_models

class NewUserForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = groggy_models.Grogger
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super().save(commit=False)
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user

class ProfileForm(forms.Form):
    grognames = forms.CharField(required=False,
            label = "Grognames",
            widget = forms.Textarea(
                attrs={
                    'class': 'form-control',

                    'rows': 8,
                    },
                ),
            )

class PostForm(forms.Form):
    grogname = forms.CharField(required=True,
            label = "From",
            widget = forms.TextInput(
                attrs={
                    'class': 'form-control',
                    },
                ),
            )

    content = forms.CharField(required=True,
            label = "Item text",
            widget = forms.Textarea(
                attrs={
                    'class': 'form-control',

                    'placeholder':
                    'Capybaras should run the whole world. '
                    'Imagine the benefits!',

                    'rows': 8,
                    },
                ),
            )

class NewPostForm(PostForm):
    title = forms.CharField(required=False,
            label = "Item title",
            widget = forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'I really like capybaras',
                    },
                ),
            )

    field_order = [
            'grogname',
            'title',
            'content',
            ]
