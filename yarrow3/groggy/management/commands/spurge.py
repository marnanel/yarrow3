import random
import hashlib
import binascii
import datetime
import textwrap
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Min
import yarrow3.groggy.models as groggy_models

INDEX_FORMAT = '%(sequence)08x %(timestamp)08x %(itemid)8s %(userid)-75s %(type)s %(subject)94s'

def random_hex_string(length = 32):
    "Generates a string of random hex digits."

    result = ''

    # The easiest way to generate a hex digit is using the built-in
    # hex() function, which returns strings of the form "0x7"-- so
    # we take the third character.
    
    for n in range(0, length):
        result = result + hex(int(random.random()*16))[2]

    return result

def inverted_bitstring(x):
    result = [255-c for c in x]
    return bytes(result)

def basic_input():
    return input()

def basic_output(s, carriage_return = True):
    if carriage_return:
        end = '\r\n'
    else:
        end = '\n'
    print(s, end=end)

def ellipsise(s):
    if len(s)>94:
        return s[:90]+'...'
    else:
        return s

def dump(s):
    with open('/tmp/spurge.log', 'a') as f:
        f.write('%s\n' % (s,))

class Command(BaseCommand):
    help = 'Runs the RGTP server.'

    def _write(self, code, message,
            carriage_return = True):
        if code is None:
            line = message
        else:
            line = '%03d %s' % (code, message)

        dump(line)
        basic_output(line,
                carriage_return = carriage_return)

    def _write_parse_fail(self):
        self._write(510, "Huh?")

    def _write_goodbye(self):
        self._write(280, 'Goodbye.')

    def _write_eof(self):
        self._write(None, '.')

    def _read(self):
        while True:

            try:
                line = basic_input().strip()
                dump(line)
            except EOFError:
                self._write_goodbye()
                return None

            if not line:
                self._write(510, "Speak up. Use QUIT to quit.")
                continue

            line = line.split(' ', 1)

            line = [
                    line[0].upper(),
                    ''.join(line[1:]),
                    ]

            if len(line[0])!=4:
                self._write_parse_fail()
                continue

            if line[0]=='NOOP':
                self._write(200, 'Nobody here but us trees.')
                continue
            elif line[0]=='MOTD':
                self._write(410, 'There is no message of the day.')
                continue
            elif line[0]=='QUIT':
                self._write_goodbye()
                return None

            return line

    def _send_index(self,
            since_id = None,
            since_date = None,
            ):
        entries = groggy_models.Entry.objects.all(). \
                order_by('id')

        # FIXME there is probably a way to do this with one query
        items = groggy_models.Item.objects.\
                all(). \
                annotate(first=Min('entry__id'))

        if since_id is not None:
            entries = entries.filter(
                id__gte = since_id,
                    )

        if since_date is not None:
            date = datetime.datetime.utcfromtimestamp(since_date)
            entries = entries.filter(
                date__date__gte = date,
                    )

        for entry in entries:

            if entry.id == items.get(id=entry.item.id).first:
                if entry.item.previous is not None:
                    type_code = 'C'
                else:
                    type_code = 'I'
            else:
                type_code = 'R'

            # Note that the spec requires the title at the time
            # the entry was posted, not at the time the index
            # was requested. But we don't store that information.
            subject = ellipsise(entry.item.title)

            index_line = {
                    'sequence': entry.id,
                    'timestamp': int(entry.date.timestamp()),
                    'itemid': entry.item.slug,
                    'userid': entry.poster.get_username(),
                    'type': type_code,
                    'subject': subject,
                }

            self._write(None, INDEX_FORMAT % index_line)

            if type_code=='C':
                # Quirk of the protocol: we send two lines with
                # the same sequence number, showing the old and
                # new items.

                cont_line = index_line.copy()
                cont_line['type'] = 'F'
                cont_line['itemid'] = entry.item.previous.slug
                cont_line['subject'] = ellipsise(entry.item.previous.title)
                self._write(None, INDEX_FORMAT % cont_line)

    def handle(self, *args, **options):

        username = None
        user = None
        server_nonce = None

        self._write(230, 'Spurge rgtpd ready')

        while True:

            line = self._read()
            if not line:
                return

            if line[0]=='REGU':
                self._write(482, 'You must register using the website.')
            elif line[0]=='USER':

                requested_access_level = 3

                params = line[1].split(' ')
                username = params[0]
                if len(params)>1 and params[1][0] in ('1', '2', '3'):
                    requested_access_level = int(params[1][0])

                try:
                    user = groggy_models.Grogger.objects.get(
                            username = username,
                            )
                except groggy_models.Grogger.DoesNotExist:
                    self._write(482, "I don't know you.")
                    continue

                self._write(130, 'MD5  Prove it.')

                server_nonce = random_hex_string()
                self._write(333, server_nonce)
                server_nonce = server_nonce.encode('ascii')

            elif line[0]=='AUTH':

                if not server_nonce:
                    self._write(500, 'Use USER first.')
                    continue

                if len(line)!=2:
                    self._write(511, 'Params must be (client hash, client nonce).')
                    continue

                fields = line[1].split(' ')
                if len(fields)!=2:
                    self._write(511, 'Params must be (client hash, client nonce).')
                    continue

                client_hash = fields[0]
                client_nonce = fields[1].encode('ascii')

                squished_userid = username.lower()[:16]
                while len(squished_userid)<16:
                    squished_userid += '\0'
                squished_userid = squished_userid.encode('ascii')

                shared_secret = user.rgtp_shared_secret.encode('ascii')

                fingerprint = hashlib.md5()
                fingerprint.update(binascii.unhexlify(client_nonce))
                fingerprint.update(binascii.unhexlify(server_nonce))
                fingerprint.update(squished_userid)
                fingerprint.update(inverted_bitstring(binascii.unhexlify(shared_secret)))

                if client_hash.lower()==fingerprint.hexdigest():
                    # yay, they're who we think they are
                    response = hashlib.md5()
                    response.update(binascii.unhexlify(server_nonce))
                    response.update(binascii.unhexlify(client_nonce))
                    response.update(squished_userid)
                    response.update(binascii.unhexlify(shared_secret))

                    # Grrr. WrenGROGGS only allows the server to respond
                    # in uppercase here, in violation of the protocol. :(
                    self._write(133, response.hexdigest().upper())
                    break
                else:
                    self._write(483, 'Sir, you are a charlatan!')
                    return

            else:
                self._write_parse_fail()

        # If we get here, they're authorised
        self._write(232, 'Authorised.')

        while True:

            line = self._read()
            if not line:
                return

            if line[0]=='INDX':

                since_id = None
                since_date = None

                index_filter = line[1]

                if index_filter:
                    try:

                        if index_filter.startswith('#'):
                            since_id = int(index_filter[1:], 16)
                        else:
                            since_date = int(index_filter, 16)

                    except IndexError:
                        pass
                    except ValueError as ve:
                        self._write(511,
                                "You need to give a hex number, "
                                "maybe with a hash before it.")
                        break

                self._write(250, 'Index follows.')

                self._send_index(
                        since_id = since_id,
                        since_date = since_date,
                        )

                self._write(None, '.')

            elif line[0]=='ITEM':

                try:
                    item = groggy_models.Item.objects.get(slug=line[1].upper())
                except groggy_models.Item.DoesNotExist:
                    self._write(410, "No such item.")
                    break

                self._write(250, "Item follows.")

                first = True

                for entry in item.entries:
                    if first:

                        item_or_reply = 'Item '+item.slug

                        self._write(None,
                                "%(prev_itemid)8s %(next_itemid)8s "
                                "%(last_edit)8s %(last_reply)8s" % {
                                    'prev_itemid': '',
                                    'next_itemid': '',
                                    'last_edit': '', # not stored
                                    'last_reply': '',
                                    })
                    else:

                        item_or_reply = 'Reply'

                    self._write(None,
                        '^%(sequence)08x %(timestamp)08x' % {
                            'sequence': entry.id,
                            'timestamp': int(entry.date.timestamp()),
                            },
                        )

                    self._write(None,
                        "%(reply)s from %(name)s %(date)s" % {
                            'reply': item_or_reply,
                            'name': entry.poster.get_username(),
                            'date': entry.date,
                            })

                    self._write(None,
                            "From %s" % (
                                entry.poster.any_grogname,
                                ))

                    if first:
                        self._write(None,
                                "Subject: %s" % (item.title,))

                        if item.previous is not None:
                            self._write(None, '')
                            self._write(None,
                                    '[Continued from %s]' % (
                                        item.previous.slug,
                                        ))

                    self._write(None, '')

                    for line in entry.content.split('\n'):
                        if line.startswith('^'):
                            line = '^'+line
                        elif line.startswith('.'):
                            line = '.'+line

                        self._write(None,
                                textwrap.fill(line),
                                carriage_return = False)

                    first = False

                    self._write(None, '')

                next_item = item.next
                if next_item is not None:
                    self._write(None,
                            '[Continued in %s]' % (
                                next_item.slug,
                                ))
                    self._write(None, '')

                self._write(None, '.')

            else:
                self._write_parse_fail()
