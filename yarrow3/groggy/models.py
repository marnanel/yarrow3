from django.db import models
import os
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import transaction
import datetime
import django.db.utils
import random
import django.utils

import logging
logger = logging.getLogger(name="yarrow3")

def _random_rgtp_shared_secret():
    return '%08X' % (random.randint(0, 0xFFFFFFFF),)

class Entry(models.Model):

    class Meta:
        verbose_name_plural = "entries"

    id = models.AutoField(primary_key=True)

    grogname = models.CharField(
            max_length = 255,
            )

    poster = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            on_delete = models.DO_NOTHING,
            )

    content = models.TextField()

    date = models.DateTimeField(
            auto_now_add=True,
            blank=True,
            )

    item = models.ForeignKey(
            'Item',
            on_delete = models.CASCADE,
            )

    def __str__(self):
        result = f'[{self.id} - {self.poster} on {self.item}]'

        return result

class Item(models.Model):

    id = models.AutoField(
            primary_key = True,
            )

    slug = models.SlugField(
            max_length = 8,
            unique = True,
            )

    title = models.CharField(
            max_length = 256,
            )

    previous = models.OneToOneField(
            'self',
            related_name = 'next_one',
            on_delete = models.DO_NOTHING,
            null = True,
            blank = True,
            )

    class FullException(Exception):
        pass

    def save(self, *args, **kwargs):

        if self.slug:
            return super().save(*args, **kwargs)

        now = datetime.datetime.utcnow()
        year_letter = chr(65+now.year%26)

        serial = \
                now.timetuple().tm_yday * 10000 + \
                now.hour * 100 + \
                now.minute

        while True:

            self.slug = '%s%07d' % (year_letter, serial)

            try:

                with transaction.atomic():
                    return super().save(*args, **kwargs)

            except django.db.IntegrityError:
                serial += 1
                # round again, then

    @property
    def next(self):
        try:
            return self.next_one
        except self.DoesNotExist:
            return None

    def __str__(self):
        if self.slug:
            return f"{self.slug} \u2014 {self.title}"

        return super().__str__()

    @property
    def is_full(self):
        count = Entry.objects.filter(item=self).count()

        if count>=settings.YARROW3_MAX_ITEM_LENGTH:
            logger.debug("%s: full; contains %d entries (max is %d)",
                    self, count,
                    settings.YARROW3_MAX_ITEM_LENGTH,
                    )
            return True

        return False

    @property
    def entries(self):
        result = list(Entry.objects.filter(
                item = self,
                ).order_by(
                        'date',
                        ))

        return result

    def count(self):
        return Entry.objects.filter(
                item = self,
                ).count()

    def continuation(self):
        """
        Creates a new, empty Item, and sets this Item to
        be its "previous". If there's already such an Item,
        returns that Item.
        """

        try:
            existing = Item.objects.get(
                    previous = self,
                    )
            return existing
        except Item.DoesNotExist:
            pass

        newbie = Item(
            previous = self,
            )
        newbie.save()

        return newbie

    @property
    def latest_date(self):
        return max([e.date for e in self.entries])

    def mark_seen_for(self, user):
        current_latest = self.latest_date

        try:
            seen = Seen.objects.get(
                    who = user,
                    what = self,
                    )

            if seen.when < current_latest:
                logger.debug("Updated Seen record: %s, %s, %s",
                        user, self, current_latest)

                seen.when = current_latest
                seen.save()

        except Seen.DoesNotExist:

            logger.debug("New Seen record: %s, %s, %s",
                    user, self, current_latest)

            Seen(
                    who = user,
                    what = self,
                    when = current_latest,
                    ).save()

    def seen_by(self, whom):

        logger.debug(f"Has {whom} seen {self}?")

        latest = self.latest_date
        caught_up = whom.caught_up

        if caught_up is not None and latest <= caught_up:
            logger.debug(f"  -- yes, it's older than their caught_up date")
            return True

        try:
            when = Seen.objects.get(
                who = whom,
                what = self,
                ).when
            result = latest <= when
            logger.debug(f"  -- {latest} <= {when}? {result}")
            return result
        except Seen.DoesNotExist:
            logger.debug(f"  -- no, they've never seen it at all")
            return False

class Grogger(AbstractUser):

    grognames = models.TextField(help_text = """This grogger's grogname--
    or, if it contains newlines, a list of grognames separated by
    newlines.

    After some consideration: this is always written and generally
    read as one string, so we'll make it one string in the database.

    The property "any_grogname" will return any random grogname
    from this list.
    """)

    caught_up = models.DateTimeField(
            null = True,
            default = None,
            help_text = """The time when this grogger most
    recently requested to mark all items seen. Any item
    updated at or before this time is considered seen,
    whatever the status of its Seen fields.
    """)

    rgtp_shared_secret = models.CharField(
            max_length = 8,
            default = _random_rgtp_shared_secret,
            verbose_name = 'RGTP shared secret',
            help_text = """A string of symbols used to
    log into this account over RGTP. If you don't know
    what this is, it doesn't matter.
    """
            )


    @property
    def any_grogname(self):

        if not self.grognames:
            return self.username

        return random.choice(self.grognames.strip().split('\n'))

    def catch_up(self):

        now = django.utils.timezone.now()

        logger.info(f"Marking {self} as caught up to {now}.")

        self.caught_up = now
        self.save()

        Seen.objects.filter(who=self).delete()

class Seen(models.Model):

    who = models.ForeignKey(
            Grogger,
            on_delete = models.CASCADE,
            )

    what = models.ForeignKey(
            Item,
            on_delete = models.CASCADE,
            )

    when = models.DateTimeField(
            auto_now_add=True,
            )

    def __str__(self):
        return f"[{self.who} saw {self.what} at {self.when}]"
