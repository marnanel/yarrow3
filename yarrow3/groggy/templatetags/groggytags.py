from django import template
from django.template.defaultfilters import stringfilter
import django.utils.safestring
import markdown as md
from markdown.inlinepatterns import InlineProcessor
from markdown.extensions import Extension
import xml.etree.ElementTree as etree
import yarrow3.groggy.models as groggy_models
import re
from markdown.preprocessors import Preprocessor

register = template.Library()

@register.simple_tag
def seen(who, what):
    """
    If Grogger "who" has seen Item "what",
    returns "seen". Otherwise, returns "unseen".
    """
    if what.seen_by(who):
        return 'seen'
    else:
        return 'unseen'

class ItemidInlineProcessor(InlineProcessor):

    def __init__(self, *args, **kwargs):
        self._first_run = True
        super().__init__(*args, **kwargs)

    def handleMatch(self, m, data):

        itemid = m.group(1).upper()

        try:
            item = groggy_models.Item.objects.get(
                    slug=itemid)

            el = etree.Element('a')
            el.attrib['href'] = f'/{itemid}' # FIXME inelegant
            el.attrib['class'] = 'item-link'
            el.attrib['title'] = item.title
            el.text = itemid
        except groggy_models.Item.DoesNotExist:
            el = etree.Element('del')
            el.attrib['class'] = 'item-link-broken'
            el.text = itemid

        return el, m.start(0), m.end(0)

class NoImagesExtension(Extension):
    def extendMarkdown(self, md):
        for dont_want in [
                'image_link',
                'image_reference',
                'short_image_ref',
                ]:
            md.inlinePatterns.deregister(dont_want)

class ItemidExtension(Extension):
    def extendMarkdown(self, md):
        ITEMID_PATTERN = r'([A-Z][0-9]{7})'
        md.inlinePatterns.register(
                ItemidInlineProcessor(
                    ITEMID_PATTERN, md),
                'itemid', 2)

# The URLify extension: Copyright (c) 2014 Selcuk Ayguney. MIT licence.
# See: https://github.com/selcuk/markdown-urlify

urlfinder = re.compile(r'((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+(:[0-9]+)?|'
                       r'(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:/[\+~%/\.\w\-_]*)?\??'
                       r'(?:[\-\+=&;%@\.\w_]*)#?(?:[\.!/\\\w]*))?)')

class URLify(Preprocessor):
    def run(self, lines):
        return [urlfinder.sub(r'<\1>', line) for line in lines]

class URLifyExtension(Extension):
    def extendMarkdown(self, md, md_globals):
        md.preprocessors.add('urlify', URLify(md), '_end')

# End URLify extension.

@register.filter
@stringfilter
def markdown(value):
    i = ItemidExtension()
    result = django.utils.safestring.mark_safe(
            md.markdown(
                value,
                extensions=[
                    'smarty',
                    NoImagesExtension(),
                    ItemidExtension(),
                    URLifyExtension(),
                    ],
                )
            )
    return result
