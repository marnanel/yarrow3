from yarrow3.groggy.models import Grogger
from yarrow3.groggy.tests import GroggyTestCase
from yarrow3.groggy.post import *
import yarrow3.groggy.models as groggy_models

TITLE = "This is a test"
BODY1 = "Hello world"
BODY2 = "Thanks for all the fish"

class TestPost(GroggyTestCase):

    def setUp(self):
        self.user1 = Grogger.objects.create_user(
                'fred',
                'fred@wombat.com',
                'pass',
                )

    def test_simple(self):

        post(f"{TITLE}\n\n{BODY1}",
                poster=self.user1,
                parent=None,
                )

        items = groggy_models.Item.objects.all()

        self.assertEqual(
                len(items),
                1,
                )

        self.assertEqual(
                items[0].title,
                TITLE,
                )

        entries = items[0].entries

        self.assertEqual(
                len(entries),
                1)

        self.assertEqual(
                entries[0].poster,
                self.user1,
                )

        self.assertEqual(
            entries[0].content,
            BODY1,
            )

    def test_no_title(self):

        with self.assertRaises(ValueError):
            post(f"",
                    poster=self.user1,
                    parent=None,
                    )

    def test_no_body(self):

        with self.assertRaises(ValueError):
            post(f"{TITLE}\n\n",
                    poster=self.user1,
                    parent=None,
                    )

    def test_post_reply(self):

        original = post(f"{TITLE}\n\n{BODY1}",
                poster=self.user1,
                parent=None,
                )

        reply = post(f"{BODY2}",
                poster=self.user1,
                parent=original,
                )

        items = groggy_models.Item.objects.all()

        self.assertEqual(
                len(items),
                1,
                )

        self.assertEqual(
                items[0].title,
                TITLE,
                )

        entries = items[0].entries

        self.assertEqual(
                len(entries),
                2)

        self.assertEqual(
                entries[0].poster,
                self.user1,
                )

        self.assertEqual(
            entries[0].content,
            BODY1,
            )

        self.assertEqual(
                entries[1].poster,
                self.user1,
                )

        self.assertEqual(
            entries[1].content,
            BODY2,
            )

    def test_force_continuation(self):

        original = post(f"{TITLE}\n\n{BODY1}",
                poster=self.user1,
                parent=None,
                )

        items=[original]
        i = 0

        while len(items)<3:
            possibly_another = post(f"This is entry {i}",
                    poster=self.user1,
                    parent=original,
                    )

            if possibly_another not in items:
                items.append(possibly_another)
            i += 1

        for candidate, full, want_prev, want_next in [
                (items[0], True, None, items[1]),
                (items[1], True, items[0], items[2]),
                (items[2], False, items[1], None),
                ]:

            self.assertEqual(
                    candidate.is_full,
                    full,
                    )

            self.assertEqual(
                    candidate.previous,
                    want_prev,
                    )

            self.assertEqual(
                    candidate.next,
                    want_next
                    )
