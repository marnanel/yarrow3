from yarrow3.groggy.tests import GroggyTestCase
import yarrow3.groggy.models as groggy_models
from django.conf import settings
from datetime import datetime
import pytz

class Tests(GroggyTestCase):

    def test_simple(self):

        user = groggy_models.Grogger(username='fred')
        user.save()

        items = []
        
        def add_entry_to(item):
            entry = groggy_models.Entry(
                    item = item,
                    poster = user,
                    grogname = 'wombat',
                    content = "Wombat").save()

        for i in range(3):
            items.append(groggy_models.Item(
                    title=f"Item {i}",
                    ))
            items[-1].save()

            add_entry_to(items[-1])
          
        for i, expected in enumerate([False, False, False]):
            self.assertEqual(
                items[i].seen_by(user),
                expected
                )

        items[1].mark_seen_for(user)

        for i, expected in enumerate([False, True, False]):
            self.assertEqual(
                items[i].seen_by(user),
                expected
                )

        add_entry_to(items[1])

        for i, expected in enumerate([False, False, False]):
            self.assertEqual(
                items[i].seen_by(user),
                expected
                )

        user.catch_up()

        for i in range(3):
            self.assertTrue(
                items[i].seen_by(user),
                )

        for i, expected in enumerate([True, True, True]):
            self.assertEqual(
                items[i].seen_by(user),
                expected
                )

        add_entry_to(items[1])

        for i, expected in enumerate([True, False, True]):
            self.assertEqual(
                items[i].seen_by(user),
                expected
                )
