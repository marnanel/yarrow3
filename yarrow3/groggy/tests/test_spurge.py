import binascii
import hashlib
import datetime
from django.core.management import call_command
from django.conf import settings
from yarrow3.groggy.tests import GroggyTestCase
import yarrow3.groggy.models as groggy_models
import yarrow3.groggy.management.commands.spurge as spurge
from yarrow3.groggy.management.commands.spurge import inverted_bitstring
from yarrow3.groggy.models import Grogger, Item, Entry

def spurge_test(fn):
    """
    Use this as a decorator on a generator. It calls the spurge
    command for you. When the generator yields a string, that
    string will be sent as input to the spurge command. The
    result will be in self._last_input_code and self._last_input_message
    the next time the generator is called.

    If you want more input without replying immediately,
    you can yield None.

    This only works on generators, so make sure there's a yield
    statement in your function somewhere.
    """
    def _wrapper(self):

        self._set_handler(fn)
        
        call_command('spurge')

    return _wrapper

class InterpretedIndex:
    """If you pass index lines into the eat() method in
the form in which they pass across the wire, this class will
interpret them, and offers many useful accessors for the data
therein."""

    def __init__(self):
        self.index = {}
        self.last_sequences = { 'all': -1 }
        self.version = 2
        self.c_line = None

    def eat(self, lines):
        for line in lines:

            sequence = int(line[0], 16)
            if sequence>self.last_sequences['all']:
                self.last_sequences['all'] = sequence

            if line[4] in ['I','R','C','F']:
                if not self.index.has_key(line[2]):
                    self.index[line[2]] = {'date': 0,
                                   'count': 0,
                                   'subject': 'Unknown',
                                   'started': int(line[1], 16),
                                   'live': 1 }
                    self.last_sequences[line[2]] = -1

                if line[4]!='F' and sequence > self.last_sequences[line[2]]:
                    self.last_sequences[line[2]] = sequence

                if line[4] in ['I','C']:
                    self.index[line[2]]['subject'] = line[5]

                if self.index[line[2]]['date'] < int(line[1],16):
                    self.index[line[2]]['date'] = int(line[1],16)
                    self.index[line[2]]['from'] = line[3]

                if line[4]!='F' and sequence > self.last_sequences[line[2]]:
                    self.last_sequences[line[2]] = sequence

                if line[4]=='C':
                    self.c_line = line

                if self.index[line[2]]['date'] < int(line[1],16):
                    self.index[line[2]]['date'] = int(line[1],16)
                    self.index[line[2]]['from'] = line[3]

                if line[4]=='F':
                    self.index[line[2]]['live'] = 0
                    self.index[line[2]]['parent'] = self.c_line[2]
                    self.index[self.c_line[2]]['child'] = line[2]

                self.index[line[2]]['count'] = self.index[line[2]]['count'] + 1

            elif line[4]=='M':
                self.last_sequences['motd'] = sequence

    def sequences(self):
        return self.last_sequences

    def keys(self):
        def compare_dates(left, right, I = self.index):
            return cmp(I[left]['date'], I[right]['date'])

        temp = self.index.keys()
        temp.sort(compare_dates)
        temp.reverse()

        return temp

class Tests(GroggyTestCase):

    def setUp(self):
        """
        Monkey-patches the basic_input() and basic_output() functions
        of the spurge command, so that the @spurge_test generator can
        do its job.
        """
        super().setUp()

        self._handler = None
        self._last_input_code = None
        self._last_input_message = None
        self._output_buffer = []
        self._reading_file = False

        def _read():
            if self._output_buffer:
                return self._output_buffer.pop(0)

            # Success!
            raise EOFError()

        def _write(s,
                carriage_return = True):

            if self._reading_file:
                message = s
                code = 0
            else:
                code, message = s.split(' ', 1)
                code = int(code)

            #print('%03d %s' % (code, message))

            self._last_input_code = code
            self._last_input_message = message

            if self._reading_file:
                if message=='.':
                    self._reading_file = False
                    self._last_input_code = 1
            else:
                if code==250:
                    self._reading_file = True

            try:
                response = self._handler.__next__()
            except StopIteration:
                return None

            if response:
                for line in response.split('\n'):
                    self._output_buffer.append(line)

        spurge.basic_input = _read
        spurge.basic_output = _write

    def _set_handler(self, handler):
        self._handler = handler(self).__iter__()

    def assertCode(self, code):
        self.assertEqual(
                code,
                self._last_input_code,
                "Expected %03d but received %03d %s" % (
                    code,
                    self._last_input_code,
                    self._last_input_message,
                    ))

    def _log_in_as(self, user):
        """
        Logs you in to the spurge server.

        "user" must be a Grogger object.
        """

        client_nonce = spurge.random_hex_string(32)

        self.assertCode(230)
        yield "USER %s" % (user.get_username(),)
        self.assertCode(130)
        self.assertTrue(
                self._last_input_message.startswith('MD5  '))
        yield None

        self.assertCode(333)
        server_nonce = self._last_input_message

        squished_userid = user.get_username().lower()[:16]
        while len(squished_userid)<16:
            squished_userid += '\0'
        squished_userid = squished_userid.encode('ascii')

        shared_secret = user.rgtp_shared_secret.encode('ascii')

        fingerprint = hashlib.md5()
        fingerprint.update(binascii.unhexlify(client_nonce))
        fingerprint.update(binascii.unhexlify(server_nonce))
        fingerprint.update(squished_userid)
        fingerprint.update(inverted_bitstring(binascii.unhexlify(shared_secret)))

        yield "AUTH %s %s" % (
                fingerprint.hexdigest(),
                client_nonce,
                )
        self.assertCode(133) # we're in, yay

        server_hash = self._last_input_message

        response = hashlib.md5()
        response.update(binascii.unhexlify(server_nonce))
        response.update(binascii.unhexlify(client_nonce))
        response.update(squished_userid)
        response.update(binascii.unhexlify(shared_secret))

        self.assertEqual(
                response.hexdigest().lower(),
                server_hash.lower(),
                msg = "The server hash is not what we were expecting.",
                )

        yield None

        self.assertCode(232)

    def _split_index(self):

        result = []
        yield None

        while self._last_input_code==0:
            b = self._last_input_message
            self.assertEqual(
                    len(b),
                    199,
                    msg="Index lines must be 199 chars long.",
                    )

            result.append(
                    (
                        int(b[0:8].strip(), 16),
                        int(b[9:17].strip(), 16),
                        b[18:26].strip(), b[27:102].strip(),
                        b[103], b[105:].strip(),
                        ),
                    )
            yield None

        self.assertEqual(self._last_input_message,
                '.',
                msg = "Index ends with a dot",
                )
        yield None

        self.assertCode(280)

        return result

    @spurge_test
    def test_noop(self):
        self.assertCode(230)
        yield "NOOP"
        self.assertCode(200)

    @spurge_test
    def test_regu(self):
        self.assertCode(230)
        yield "REGU"
        self.assertCode(482)

    @spurge_test
    def test_quit(self):
        self.assertCode(230)
        yield "QUIT"
        self.assertCode(280)

    @spurge_test
    def test_fake_user(self):
        self.assertCode(230)
        yield "USER noseybonk"
        self.assertCode(482)

    @spurge_test
    def test_failed_login(self):
        user = Grogger(username='fred')
        user.save()

        self.assertCode(230)
        yield "USER fred"
        self.assertCode(130)
        self.assertTrue(
                self._last_input_message.startswith('MD5  '))
        yield None

        self.assertCode(333)

        yield "AUTH %s %s" % (
                spurge.random_hex_string(32),
                spurge.random_hex_string(32),
                )
        self.assertCode(483)

    @spurge_test
    def test_successful_login(self):
        user = Grogger(username='fred')
        user.save()

        yield from self._log_in_as(user)
        self.assertCode(232) # check _log_in_as() actually did something
        yield None

    @spurge_test
    def test_motd(self):
        user = Grogger(username='fred')
        user.save()

        yield from self._log_in_as(user)
        yield "MOTD"
        self.assertCode(410)

    @spurge_test
    def test_indx_empty(self):
        user = Grogger(username='fred')
        user.save()

        yield from self._log_in_as(user)
        yield "INDX"
        self.assertCode(250)

        yield None
        self.assertCode(1)
        self.assertEqual(self._last_input_message, '.')

        yield None
        self.assertCode(280)

    @spurge_test
    def test_indx_plain(self):
        user = Grogger(username='fred')
        user.save()

        item1 = Item(
                title='Keeping two chevrons apart',
                )
        item1.save()

        entry1 = Entry(
                item = item1,
                poster = user,
                grogname = 'The Wombat',
                content = '',
                )
        entry1.save()

        yield from self._log_in_as(user)
        yield "INDX"
        self.assertCode(250)

        index = yield from self._split_index()
        self.assertCode(280)

        self.assertEqual(
                index,
                [(
                    1,
                    int(entry1.date.timestamp()),
                    item1.slug,
                    user.get_username(),
                    'I',
                    item1.title,
                    )],
                )

    def _general_index_test(self,
            command,
            slice_start = 0,
            ):

        user = Grogger(username='fred')
        user.save()

        items = []
        entries = []

        for i in range(2):
            item = Item(
                    title=f'Item {i}',
                    )
            item.save()
            items.append(item)

            for j in range(12):
                entry = Entry(
                        item = item,
                        poster = user,
                        grogname = f'test {i} - {j}',
                        content = f'test {i} - {j}',
                        )
                entry.save()

                entry.date = datetime.datetime(
                            year = 1980+i,
                            month = j+1,
                            day = 1,
                            tzinfo = datetime.timezone.utc,
                            )
                entry.save()

                entries.append(entry)

                if j==0 and i>0:
                    # first entry of a continuation will
                    # appear in the index twice in succession
                    entries.append(entry)

        items[1].previous = items[0]
        items[1].save()

        yield from self._log_in_as(user)
        yield command
        self.assertCode(250)

        index = yield from self._split_index()
        self.assertCode(280)

        previous_entry = None

        for index, entry in zip(index, entries[slice_start:]):

            itemid = entry.item.slug
            # TODO test for entry titles being too long to
            # appear in the index
            subject = entry.item.title

            if entry == entry.item.entries[0]:
                # first entry of an item

                if entry.item.previous is not None:

                    # first entry of a continuation

                    if entry == previous_entry:

                        # first entries of continuations appear
                        # in the index twice
                        type_code = 'F'
                        itemid = entry.item.previous.slug
                        subject = entry.item.previous.title
                    else:
                        type_code = 'C'
                else:
                    type_code = 'I'
            else:
                type_code = 'R'

            self.assertEqual(
                    index,
                    (
                        entry.id,
                        int(entry.date.timestamp()),
                        itemid,
                        entry.poster.get_username(),
                        type_code,
                        subject,
                        ),
                    )

            previous_entry = entry

    @spurge_test
    def test_indx_complex(self):
        yield from self._general_index_test(
                "INDX",
                )

    @spurge_test
    def test_indx_complex_from_date(self):
        yield from self._general_index_test(
                "INDX 14105400",
                slice_start = 8,
                )

    @spurge_test
    def test_indx_complex_from_id(self):
        yield from self._general_index_test(
                "INDX #0000000e",
                slice_start = 14,
                )

    @spurge_test
    def test_item_request(self):
        user = Grogger(username='fred')
        user.save()

        entries = []

        item = Item(
                title=f'I like wombats',
                slug="T1234567",
                )
        item.save()

        for j in range(12):
            entry = Entry(
                    item = item,
                    poster = user,
                    grogname = f'Wombat number {j}',
                    content = f'I am wombat number {j}.',
                    )
            entry.save()
            entries.append(entry)

            entry.date = datetime.datetime(
                        year = 1980+j,
                        month = 1,
                        day = 1,
                        tzinfo = datetime.timezone.utc,
                        )
            entry.save()

        yield from self._log_in_as(user)
        yield "ITEM "+item.slug

        self.assertCode(250)
        yield None

        self.assertCode(0)
        self.assertEqual(
                self._last_input_message,
                ' ' * 35,
                msg='First line is 35 spaces',
                )
        yield None

        expected_item = """^00000001 12cea600
Item T1234567 from fred 1980-01-01 00:00:00+00:00
From fred
Subject: I like wombats

I am wombat number 0.

^00000002 14b12b00
Reply from fred 1981-01-01 00:00:00+00:00
From fred

I am wombat number 1.

^00000003 16925e80
Reply from fred 1982-01-01 00:00:00+00:00
From fred

I am wombat number 2.

^00000004 18739200
Reply from fred 1983-01-01 00:00:00+00:00
From fred

I am wombat number 3.

^00000005 1a54c580
Reply from fred 1984-01-01 00:00:00+00:00
From fred

I am wombat number 4.

^00000006 1c374a80
Reply from fred 1985-01-01 00:00:00+00:00
From fred

I am wombat number 5.

^00000007 1e187e00
Reply from fred 1986-01-01 00:00:00+00:00
From fred

I am wombat number 6.

^00000008 1ff9b180
Reply from fred 1987-01-01 00:00:00+00:00
From fred

I am wombat number 7.

^00000009 21dae500
Reply from fred 1988-01-01 00:00:00+00:00
From fred

I am wombat number 8.

^0000000a 23bd6a00
Reply from fred 1989-01-01 00:00:00+00:00
From fred

I am wombat number 9.

^0000000b 259e9d80
Reply from fred 1990-01-01 00:00:00+00:00
From fred

I am wombat number 10.

^0000000c 277fd100
Reply from fred 1991-01-01 00:00:00+00:00
From fred

I am wombat number 11.
""".split('\n')

        while self._last_input_code==0:
            expected = expected_item.pop(0)

            self.assertEqual(
                    self._last_input_message,
                    expected,
                    )
            yield None

        self.assertCode(1)
        yield None
