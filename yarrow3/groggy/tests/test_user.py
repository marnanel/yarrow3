import binascii
import hashlib
import datetime
from django.core.management import call_command
from django.conf import settings
from yarrow3.groggy.tests import GroggyTestCase
import yarrow3.groggy.models as groggy_models
import yarrow3.groggy.management.commands.spurge as spurge
from yarrow3.groggy.management.commands.spurge import inverted_bitstring

class Tests(GroggyTestCase):

    def test_any_grogname(self):

        fred = groggy_models.Grogger(username="fred")
        fred.save()

        self.assertEqual(
                fred.get_username(),
                "fred")

        self.assertEqual(
                fred.grognames,
                '',
                )

        self.assertEqual(
                fred.any_grogname,
                "fred")

        fred.grognames = "Tom\nDick\nHarry"
        fred.save()

        names = ['Tom', 'Dick', 'Harry']

        for i in range(100):
            self.assertIn(
                    fred.any_grogname,
                    names)

        fred.grognames = "Tom\nDick\nHarry\n"
        fred.save()

        for i in range(100):
            self.assertIn(
                    fred.any_grogname,
                    names)

        fred.grognames = ""
        fred.save()

        self.assertEqual(
                fred.any_grogname,
                "fred")

    def test_rgtp_shared_secret(self):

        # Only testing that it exists and is valid.
        # Actual tests using it are in test_spurge.py

        fred = groggy_models.Grogger(username="fred")
        fred.save()

        value = int(
                fred.rgtp_shared_secret,
                16)
