from yarrow3.groggy.tests import GroggyTestCase
import yarrow3.groggy.models as groggy_models
from yarrow3.groggy.models import Grogger
from django.conf import settings

class Tests(GroggyTestCase):

    def test_simple(self):

        user = Grogger(username='fred')
        user.save()

        item = groggy_models.Item()
        item.save()

        entry = groggy_models.Entry(
                item = item,
                poster = user,
                grogname = 'wombat',
                content = """I like bananas
Monkey nuts
And grapes""").save()
       
        self.assertEqual(
                item.entries[0].content,
                """I like bananas
Monkey nuts
And grapes""")

    def test_multi_entries(self):
        wombat = Grogger(username='wombat')
        wombat.save()

        fred = Grogger(username='fred')
        fred.save()

        jane = Grogger(username='jane')
        jane.save()

        item = groggy_models.Item()
        item.save()

        groggy_models.Entry(
                item = item,
                poster = wombat,
                grogname = 'wombat',
                content = """I like bananas
Monkey nuts
And grapes""").save()

        groggy_models.Entry(
                item = item,
                poster = fred,
                grogname = 'fred',
                content = "Gosh, so do I",
                ).save()

        groggy_models.Entry(
                item = item,
                poster = jane,
                grogname = 'jane',
                content = "Who knew?").save()

        self.assertEqual(
                len(item.entries),
                3)
       
        self.assertEqual(
                item.count(),
                3)

        self.assertEqual(
                item.entries[2].grogname,
                'jane')

        self.assertEqual(
                item.entries[2].content,
                "Who knew?")

    def test_itemid_clash(self):
        user = Grogger(username='fred')
        user.save()

        for i in range(10):
            item = groggy_models.Item()
            item.save()

            entry = groggy_models.Entry(
                    item = item,
                    poster = user,
                    grogname = 'wombat',
                    content = """I like bananas
    Monkey nuts
    And grapes""").save()

        self.assertTrue(True, "Rapid item creation didn't crash")
