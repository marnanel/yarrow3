from django.urls import path, re_path
import django.contrib.auth.views
from django.contrib.auth.decorators import login_required
import yarrow3.groggy.views as groggy_views

urlpatterns = [
        path('',
            groggy_views.RootPage.as_view()),

        path('g/main', # explicitly hardcoded for now
            groggy_views.IndexPage.as_view()),

        path('register',
            groggy_views.RegisterPage.as_view()),

        path('login',
            groggy_views.LoginPage.as_view(),
            name='login',
            ),

        path('logout',
            groggy_views.LogoutPage.as_view()),

        path('password',
            groggy_views.PasswordPage.as_view()),

        path('catchup',
            groggy_views.CatchupPage.as_view()),

        path('profile',
            login_required(
                groggy_views.ProfilePage.as_view(),
                )),

        path('post',
            login_required(
                groggy_views.PostPage.as_view(),
                )),

        re_path(r'^(?P<itemid>[A-Z][0-9]{7})$',
            groggy_views.ItemPage.as_view()),

        ]
