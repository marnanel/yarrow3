import random
from django.views import View
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.db.models import Max
from django.contrib.messages.views import SuccessMessageMixin
import django.contrib.auth.views as auth_views
import yarrow3.groggy.models as groggy_models
from yarrow3.groggy.forms import *
from yarrow3.groggy.post import post

import logging
logger = logging.getLogger(name='yarrow3')

class RootPage(View):

    def get(self, request, *args, **kwargs):

        logger.info("Serving root page")

        result = render(
                request=request,
                template_name='root-page.html',
                context = {
                    },
                )

        return result

class IndexPage(View):

    def get(self, request, *args, **kwargs):

        if not request.user.is_authenticated:
            return redirect('/')

        logger.info("Serving index page")

        items = groggy_models.Item.objects.\
                all(). \
                annotate(updated=Max('entry__date')). \
                order_by('-updated')

        result = render(
                request=request,
                template_name='index-page.html',
                context = {
                    "items": items,
                    },
                )

        return result

class ItemPage(View):

    def get(self, request,
            itemid,
            form = None,
            *args, **kwargs):

        logger.info("Serving item page %s",
                itemid)

        this_item = get_object_or_404(groggy_models.Item, slug=itemid)

        this_item.mark_seen_for(request.user)

        context = {
                'item': this_item,
                'entries': this_item.entries,
                'prev': this_item.previous,
                'next': this_item.next,
                'full': this_item.is_full,
                }

        if form:
            context['form'] = form
        elif not context['next']:
            if this_item.is_full:
                context['form'] = NewPostForm(
                        initial = {
                            'grogname': request.user.any_grogname,
                            },
                        )
            else:
                context['form'] = PostForm(
                        initial = {
                            'grogname': request.user.any_grogname,
                            },
                        )

        result = render(
                request=request,
                template_name='item.html',
                context = context,
                )

        return result

    def post(self, request,
            itemid,
            *args, **kwargs):

        this_item = get_object_or_404(groggy_models.Item, slug=itemid)

        # NewPostForm should receive both possible forms correctly
        form = NewPostForm(request.POST)

        if not form.is_valid():
            return self.get(request,
                    itemid)

        title = form.cleaned_data.get('title')
        grogname = form.cleaned_data.get('grogname')
        content = form.cleaned_data.get('content')

        if title:
            content = f"{title}\n{content}"

        if this_item.is_full and not title:
            messages.error(request, "You must give a title, because "
            "this is a new item.")
        else:

            try:
                item = post(
                    poster = request.user,
                    grogname = grogname,
                    content = content,
                    parent = this_item,
                    )
                messages.info(request, "Posted.")

                return redirect(f"/{item.slug}") # FIXME ugly

            except AttributeError as ve:
                messages.error(request, f"{ve}")

        return self.get(request,
                itemid, form)

class RegisterPage(View):

    def post(self, request):

        form = NewUserForm(request.POST)

        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registration successful." )
            logger.info("New user created: %s",
                    user)
            return redirect("/")

        # TODO this message is less than helpful
        messages.error(request, "Unsuccessful registration. Invalid information.")
        logger.info("Invalid attempt to create user")
        return self.get(request)

    def get(self, request):
        form = NewUserForm
        return render(request=request,
                template_name="register.html",
                context={"register_form":form},
                )

class ErrorMessageMixin(View):
    def form_invalid(self, form):
        messages.error(self.request, self.error_message)
        return super().form_invalid(form)

class LoginPage(SuccessMessageMixin, ErrorMessageMixin, auth_views.LoginView):
    template_name = 'login.html'
    success_url = '/g/main'
    success_message = "You are now logged in as %(username)s."
    error_message = 'Incorrect username or password.'

class LogoutPage(auth_views.LogoutView):
    next_page = '/'

class PasswordPage(SuccessMessageMixin, ErrorMessageMixin, auth_views.PasswordChangeView):
    template_name = 'password.html'
    success_url = '/g/main'
    success_message = "Your password has been changed."
    error_message = "That didn't work."

class ProfilePage(View):

    def post(self, request):
        form = ProfileForm(request.POST)

        if not form.is_valid():
            return self.get(request)

        grognames = form.cleaned_data.get('grognames')

        logger.info("%s sets their grognames to: %s",
                request.user, grognames)

        request.user.grognames = grognames
        request.user.save()

        messages.info(request, "Your profile has been updated.")

        return redirect("/profile")

    def get(self, request):
        form = ProfileForm(
                initial = {
                    'grognames': request.user.grognames,
                    },
            )

        return render(request=request,
                template_name="profile.html",
                context={
                    "form": form,
                    },
                )

class CatchupPage(View):

    def get(self, request):
        logger.info("Catch up: %s", request.user)
        request.user.catch_up()
        messages.info(request, "I have marked all items as read.")
        return redirect("/g/main")

class PostPage(View):

    def get(self, request):

        form = NewPostForm(
            initial = {
                'grogname': request.user.any_grogname,
                },
            )

        return render(request=request,
                template_name="post.html",
                context={
                    "form": form,
                    },
                )

    def post(self, request):
        form = NewPostForm(request.POST)

        if not form.is_valid():
            return self.get(request)

        grogname = form.cleaned_data.get('grogname')
        title = form.cleaned_data.get('title')
        content = form.cleaned_data.get('content')

        if not title:
            messages.error(request, "You must give a title.")
        else:

            try:
                item = post(
                    poster = request.user,
                    grogname = grogname,
                    content = f"{title}\n{content}",
                    )
                messages.info(request, "Posted.")

                return redirect(f"/{item.slug}") # FIXME ugly

            except ValueError as ve:
                messages.error(request, f"{ve}")

        return self.get(request)
